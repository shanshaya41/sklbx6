﻿// 19HW.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <vector>
using namespace std;

class Animal {
public:
    virtual void Voice() {
        cout << "This is parent text" << endl;
    }
};

class Dog :public Animal {
public:
    void Voice() override {
        cout << "Woof!" << endl;
    }
};
    
class Cat : public Animal {
public:
    void Voice() override {
        cout << "Meow!" << endl;
    }

};

class Cow : public Animal {
public:
    void Voice() override {
        cout << "Moo!" << endl;
    }
};


int main()
{


    Animal* array[10];

    Cow cow;
    Cat cat;
    Dog dog;
    for (int i = 0; i < 10; i++) {
        int r = rand() % 3;
        switch (r) {
        case 0: {
            array[i] = &dog;
            break;
        }
        case 1: {
            array[i] = &cat;
            break;
        }
        case 2: {
            array[i] = &cow;
            break;
        }
        }
    }
    for (int i = 0; i < 10; i++) {
        array[i]->Voice();

    }
}

